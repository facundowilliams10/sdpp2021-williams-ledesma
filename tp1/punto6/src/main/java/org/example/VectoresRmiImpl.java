package org.example;

//Implementa la interfaz remota
public class VectoresRmiImpl implements VectoresRmi {
    int temperatura = (int) Math.floor(Math.random()    *(30-0+1)+0);
    int probPrecipitaciones = (int) Math.floor(Math.random()*(100-0+1)+0);
    static int humedad = (int) Math.floor(Math.random()*(100-0+1)+0);
    int viento = (int) Math.floor(Math.random()*(10-0+1)+0);

    // implementacion de metodos
    @Override
    public int[] sumarVectores(int[] v1, int[] v2){
        int[] vResultado = new int[v1.length];
        for (int i = 0; i < v1.length; i++)
            vResultado[i]= v1[i]+v2[i];
        return vResultado;        
    }
    public int[] restarVectores(int[] v1, int[] v2){
        int[] vResultado = new int[v1.length];
        for (int i = 0; i < v1.length; i++)
            vResultado[i]= v1[i]-v2[i];
        return vResultado;          
    }
    public int[] sumarVectoresModificando(int[] v1, int[] v2){
        int[] vResultado = new int[v1.length];
        for (int i = 0; i < v1.length; i++){
            vResultado[i]= v1[i]+v2[i];
            v1[i]=0;
            v2[i]=0;
        }
        return vResultado;              
    }
    public int[] restarVectoresModificando(int[] v1, int[] v2){
        int[] vResultado = new int[v1.length];
        for (int i = 0; i < v1.length; i++){
            vResultado[i]= v1[i]-v2[i];
            v1[i]=0;
            v2[i]=0;
        }
        return vResultado;              
    }
}