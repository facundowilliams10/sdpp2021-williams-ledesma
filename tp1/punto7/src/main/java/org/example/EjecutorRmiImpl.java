package org.example;

//Implementa la interfaz remota
public class EjecutorRmiImpl implements EjecutorRmi {
    // Devuelve ejecucion de la tarea generica
    @Override
    public double ejecutar(Tarea t){
        return t.ejecutar();
    }
}