package org.example;


import java.net.ServerSocket;
import java.net.Socket;
import org.example.Message;
import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class ServerTcp
{
    public ServerTcp(int port){
        /*
        Capa de Trasnporte: TCP / UDP
         */
        try{
            ServerSocket ss = new ServerSocket(port);
            
            System.out.println("Punto 4: Server has started on port "+port);
            List<Message> mensajes = new ArrayList<>();
            List<String> users = new ArrayList<String>();
        
            while (true){
                Socket client = ss.accept();
                /*
                Particular de java como maneja la E/S del socket ->
                                                    Canal de Entrada -> server (lee)
                                                    Canal de Salida -> server  (escribe)
                --> Canal
                    * String <-- JSON <-- TEXT
                    * Buffer
                    * Object Serializable (JAVA) --> Public class Auto implements Serializable {}
                 */
                //Message mensaje = new Message("123", "123", "body");
                //mensajes.add(mensaje);
                //System.out.println(mensajes.get(0).getBody());
                System.out.println("Atendiendo al cliente: "+client.getPort());

                // 3 pasos
                // 1er paso
                ServerHilo sh = new ServerHilo(client, mensajes, users);
                // 2do paso
                Thread serverThread = new Thread(sh);
                // 3er paso
                serverThread.start();

            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    public static void main( String[] args )
    {
        // parametros de consola
        int port = 9090;
        ServerTcp server = new ServerTcp(port);
    }
}
