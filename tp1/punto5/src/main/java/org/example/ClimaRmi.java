package org.example;

import java.rmi.Remote;

//interfaz remota para la aplicación
public interface ClimaRmi extends Remote {
    //métodos de la interfaz remota
    String infoClimaRmi() throws java.rmi.RemoteException;
}