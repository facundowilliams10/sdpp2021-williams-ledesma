package org.example;

import java.rmi.Remote;

//interfaz remota para la aplicación
public interface EjecutorRmi extends Remote {
    //métodos de la interfaz remota
    public double ejecutar(Tarea t) throws java.rmi.RemoteException;
}