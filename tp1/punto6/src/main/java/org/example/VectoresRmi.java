package org.example;

import java.rmi.Remote;

//interfaz remota para la aplicación
public interface VectoresRmi extends Remote {
    //métodos de la interfaz remota
    int[] sumarVectores(int[] v1, int[] v2) throws java.rmi.RemoteException;
    int[] restarVectores(int[] v1, int[] v2) throws java.rmi.RemoteException;
    int[] sumarVectoresModificando(int[] v1, int[] v2) throws java.rmi.RemoteException;
    int[] restarVectoresModificando(int[] v1, int[] v2) throws java.rmi.RemoteException;

}