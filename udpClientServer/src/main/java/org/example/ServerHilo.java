package org.example;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ServerHilo implements Runnable {
    DatagramPacket sendPacket;
    DatagramSocket client;
    byte[] sendData = new byte[8];

    public ServerHilo(DatagramSocket client) {

        this.client = client;
    }

    @Override
    public void run() {
        // imaginando un proceso que lleva tiempo
        try {
            Thread.sleep(2000);
            String sendString = "el_server_responde_con_un_datagrama_UDP!";
            byte[] sendData = sendString.getBytes("UTF-8");
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, client.getInetAddress(), client.getPort());
            client.send(sendPacket);


            client.close();
        }catch (Exception e){

        }


    }
}