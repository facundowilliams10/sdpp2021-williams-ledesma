package org.example;

public class Message {
    private String from; 
    private String to;
    private String body;

    public Message(String from, String to, String body ){
        this.body = body;
        this.to = to;
        this.from = from;
    }
    public String getTo(){
        return this.to;
    }
    public String getBody(){
        return this.body;
    }
    public String getFrom(){
        return this.from;
    }

}