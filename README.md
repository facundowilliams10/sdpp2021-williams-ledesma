# sdypp2021
**TP 1**

- Los archivos .jar para ejecutar los ejercicios se encuentran en tp1/archivos-jar.
- Los puntos que requieren una respuesta teórica contienen un documento word con la respuesta.
- Para cada punto dentro de la carpeta correspondiente ejecutar el siguiente comando en consola para crear la imagen docker y ejecutarla la misma:
`sh deploy-automation.sh`
