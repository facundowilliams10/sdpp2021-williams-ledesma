package org.example;


import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * Hello world!
 *
 */
public class ServerUdp
{
    
    private DatagramSocket serverSocketUdp;
    private byte[] buf = new byte[256];

    public ServerUdp(int port){
        /*
        Capa de Trasnporte: TCP / UDP
         */
        try{
            serverSocketUdp = new DatagramSocket(port);
            System.out.println("Server has started on port "+port);


            while (true){
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                serverSocketUdp.receive(packet);
                
                //me guardo la ip y puerto del cliente
                InetAddress clientAddress = packet.getAddress();
                int clientPort = packet.getPort();
                //armo un socket udp con la ip y puerto del cliente
                DatagramSocket client = new DatagramSocket(clientPort,clientAddress);
                
                /*
                Particular de java como maneja la E/S del socket ->
                                                    Canal de Entrada -> server (lee)
                                                    Canal de Salida -> server  (escribe)
                --> Canal
                    * String <-- JSON <-- TEXT
                    * Buffer
                    * Object Serializable (JAVA) --> Public class Auto implements Serializable {}
                 */
                System.out.println("Atendiendo al cliente: "+client.getPort());

                // 3 pasos
                // 1er paso
                ServerHilo sh = new ServerHilo(client);
                // 2do paso
                Thread serverThread = new Thread(sh);
                // 3er paso
                serverThread.start();

            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }
    public static void main( String[] args )
    {
        // parametros de consola
        int port = 9090;
        ServerUdp server = new ServerUdp(port);
    }
}
